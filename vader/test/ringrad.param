# Parameter file for Pringle (1981) viscous ring test, as modified to
# include radiation pressure

# General parameters
nr	    	= 4096
rmin		= 1.5e10
rmax		= 1.5e12
grid_type	= linear
alpha		= c_func
rot_curve_type	= keplerian
rot_curve_mass	= 5.97e33      # 3 Msun
ibc_pres_type	= fixed_torque
ibc_pres_val	= c_func
ibc_enth_type	= fixed_value
obc_pres_type	= fixed_torque
obc_pres_val	= c_func
obc_enth_type	= fixed_value
gamma		= c_func
dt_min		= 1e-20
dt_tol		= 1.0
interp_order	= 1
method		= BE
verbosity	= 1

# Problem-specific parameters
ring_mass	= 1.99e27	# mass of ring
init_teff	= 1e4		# temperature (not important to solution)
col_ratio	= 1e10          # ratio of column densities in and out of ring
ring_loc	= 7.5e11	# initial radius of ring
kinematic_visc	= 1.483e11      # chosen so dimensionless time t_s = 1e4 yr
end_time	= 0.128         # simulation run time in units of t_s
f_z0		= 7.5e9		# vertical scale height times f factor
